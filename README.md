# Perform gradient descent to fit several models to air quality data

* Implements a variety of functions using gradient descent:
  - Linear Model
  - Quadratic Model
  - Cubic Model
  - Quartic Model
 
- Written in python with a Jupyter Notebook
  - Libraries: numpy, pandas, matplotlib

* Uses the Air Quality Data Set
  - Downloaded from the [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Air+Quality)
  - First 43 samples without missing data